
import glfw
from OpenGL.GL import *
from OpenGL.GLU import *
from OpenGL.GLUT import *

import numpy as np

class Scene:
    """ OpenGL 2D scene class """
    # initialization
    def __init__(self, width, height,
                points=[np.array([0, 0, 0, 1])],
                scenetitle="2D Scene"):
        # time
        self.t = 0
        self.dt = 0.001
        self.scenetitle = scenetitle
        self.pointsize = 7
        self.linewidth = 3
        self.width = width
        self.height = height
        self.points = points
        self.animate = False
        self.alpha = 0 # rotation angle around x-axis
        self.beta = 0  # rotation angle around y-axis
        self.gamma = 0 # rotation angle around z-axis


    # set scene dependent OpenGL states
    def setOpenGLStates(self):
        glPointSize(self.pointsize)
        glLineWidth(self.linewidth)


    # animation
    def animation(self):
        if self.animate:
            self.beta += 10
            if self.beta > 360:
                self.beta = 0

    def render(self):
        self.animation()

        # TODO :
        # - define model view transformation matrices
        # - define projection matrix
        # - combine matrices to model view projection matrix

        w = self.width * .5
        h = self.height * .5

        # TODO: Start
        # perspektiven matritze?
        mat_v = np.array([
            [1, 0, 0, 0 ],
            [0, 1, 0, 0],
            [0, 0, 1, 0],
            [0, 0, 0, 1],
        ])

        # Orthographic Projection (aus 3d wird 2d)
        mat_size = np.array([
            [1, 0, 0, 0],
            [0, 1, 0, 0],
            [0, 0, 0, 0],
            [0, 0, 0, 1],
        ])

        # Matritze um aus boundingbox -1 / 1 auf viewport abzubilden
        mat_viewport = np.array([
            [w, 0, 0, w],
            [0, h, 0, h],
            [0, 0, .5, .5],
            [0, 0, 0, 1],
        ])

        alpha = np.deg2rad(self.alpha)
        s = np.sin(alpha)
        c = np.cos(alpha)

        mat_rot_x = np.array([
            [1, 0, 0, 0],
            [0, c, -s, 0],
            [0, s, c, 0],
            [0, 0, 0, 1],
        ])

        beta = np.deg2rad(self.beta)
        s = np.sin(beta)
        c = np.cos(beta)

        mat_rot_y = np.array([
            [c, 0, s, 0],
            [0, 1, 0, 0],
            [-s, 0, c, 0],
            [0, 0, 0, 1]
        ])

        gamma = np.deg2rad(self.gamma)
        s = np.sin(gamma)
        c = np.cos(gamma)

        mat_rot_z = np.array([
            [c, -s, 0, 0],
            [s, c, 0, 0],
            [0, 0, 1, 0],
            [0, 0, 0, 1]
        ])

        mat_pipeline = mat_size.dot(mat_v).dot(mat_viewport).dot(mat_rot_x).dot(mat_rot_y).dot(mat_rot_z)

        # TODO: Stop
        # set color to blue
        glColor(0.0, 0.0, 1.0)

        # render points
        glBegin(GL_POINTS)
        for p in self.points:
            p = mat_pipeline.dot(p)

            # render point
            glVertex4fv(p)
        glEnd()
