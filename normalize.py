import numpy as np

def bounding_box(vertices):
    x, y, z = tuple(sorted(vertices, key=lambda v: v[i]) for i in range(3))
    return np.array([
        [x[0][0], x[-1][0]],
        [y[0][1], y[-1][1]],
        [z[0][2], z[-1][2]],
    ])

def normalize(vertices):
    bb = bounding_box(vertices)

    cx, cy, cz = tuple((dim[1] - abs(dim[0])) / 2 for dim in bb)
    lx, ly, lz = tuple(dim[1] - dim[0] for dim in bb)

    mat_t = np.array([
        [1, 0, 0, -cx],
        [0, 1, 0, -cy],
        [0, 0, 1, -cz],
        [0, 0, 0, 1]
    ])

    mat_s = np.array([
        [(2 / lx), 0, 0, 0],
        [0, (2 / ly), 0, 0],
        [0, 0, -(2 / lz), 0],
        [0, 0, 0, 1]
    ])

    mat = mat_s.dot(mat_t)

    return [mat.dot(v) for v in vertices]